#!/usr/bin/env python3

"""Search through given DOIs for common references."""

import argparse
import asyncio
import datetime
import select
import sys
import threading

from collections import OrderedDict
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path

import matplotlib.pyplot as plt
import networkx as nx

from pybliometrics.scopus import AbstractRetrieval, ScopusSearch
from pybliometrics.scopus.exception import ScopusException


class NodeLut(dict):
    """Lookup table for { node.id: Node } pairs"""

    def relabel(self):
        """Go through labels and make sure they are unique.

        E.g. two same node labels 'node1' and 'node1' will be renamed to
        'node1a' and 'node1b', based on comparison of their Node objects.
        """

        label_nodes = {}
        for (nid, node) in self.items():
            label = node.label
            try:
                label_nodes[label].append(node)
            except KeyError:
                label_nodes[label] = [node]

        for (label, nodes) in label_nodes.items():
            if len(nodes) > 1:
                append = "a"
                for node in sorted(nodes):
                    self[node.id].label = node.label + append
                    append = chr(ord(append) + 1)


    def update_nodes(self, nodes):
        """Add nodes into the node LUT.

        If a key (node.id) is present, it will be overwritten.
        """

        for node in nodes:
            self.setdefault(node.id, node)


# Global nodes lookup table
NODE_LUT = NodeLut()
NODE_LUT.clear()


# Global lock for multithreading
LOCK = threading.Lock()


class SearchType:
    """Base class for all search types"""
    pass


class ScSearch(SearchType):
    """Regular Scopus search, nothing special required"""
    pass


class ScAbstract(SearchType):
    """Scopus AbstractRetrieval, need to specify search_type or leave it
    default for auto-choose."""

    def __init__(self, id_type=None):
        self.id_type = id_type


def cli():
    """Parse CLI arguments and return them."""

    desc = __doc__
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-i', required=False, nargs='+',
                        help="Input file(s) with DOIs.")
    parser.add_argument('-l', '--levels', required=False, default=1, type=int,
                        help="How many times unpack references from the"
                             " starting set (default 1).")
    parser.add_argument('-v', '--verbose', required=False, action='count',
                        default=0, help="Increase the level of spam.")
    parser.add_argument('-f', '--force', action='store_true',
                        help="Do not stop on invalid HTTP queries.")
    parser.add_argument('-s', '--search-limit', required=False, default=4,
                        type=int,
                        help="How many HTTP requests to send in parallel."
                             " More is faster but can exceed quota.")
    parser.add_argument('-c', '--csv', required=False, default=None, type=str,
                        help="File to store output results.")
    parser.add_argument('-e', '--exclude', required=False, default=None,
                        type=str,
                        help="File with terms excluded from the report. Term"
                             " type string (e.g. doi, eid, ...) is assumed to"
                             " be on the first line.")
    parser.add_argument('--nocache', action='store_true',
                        help="Do not use cached search results.")
    parser.add_argument('-g', '--graph', action='store_true',
                        help="Generate citation network graph.")
    return parser.parse_args()


def has_stdin():
    """Check whether there is something in stdin"""

    if select.select([sys.stdin],[],[],0.0)[0]:
        return True
    else:
        return False


def err(*args, **kwargs):
    print("Error:", *args, file=sys.stderr, **kwargs)
    return 1


def warn(*args, **kwargs):
    print("Warning:", *args, file=sys.stderr, **kwargs)
    return 1


def read_dois(inputs):
    """Read DOIs from all 'inputs', remove all duplicates.

    Each input  an be either a file or stdin. DOIs should one on each line.

    Args:
        inputs: file paths or sys.stdin

    Returns:
        set of str: File/URL contents as a UTF-8 encoded strings
    """

    dois = []
    for inp in inputs:
        if inp is sys.stdin:
            s = inp.read()
        else:
            s = open(inp, 'r').read()
        dois += s.split('\n')

    return {doi for doi in dois if all([not doi.isspace(), doi != ''])}


class Database:
    """General API for interacting with a database (e.g. Scopus)."""

    def __init__(self, search_limit=4):
        self._search_buf = set()  # Buffer to store search results
        self._search_limit = search_limit

    def _search_term(self, term, search_type):
        """Search for 'term' in a database.

        'term' should be a unique identificator such as DOI or EID - that is
         specified by 'search_type'.

        Stores results into _search_buf attribute
        """
        raise NotImplementedError("Method '_search_term' not implemented")

    async def _search_async(self, terms, search_type):
        """Asynchronous implementation of self.search().

        Implementation from
        http://skipperkongen.dk/2016/09/09/
          easy-parallel-http-requests-with-python-and-asyncio
        """
        terms = set(terms)  # remove duplicates
        self.reset_search_buf()

        max_workers = max(min(self._search_limit, len(terms)), 1)
        with ThreadPoolExecutor(max_workers=max_workers) as ex:
            loop = asyncio.get_event_loop()
            futures = []
            for term in terms:
                futures.append(
                    loop.run_in_executor(
                        ex,                 # executor
                        self._search_term,  # func
                        term, search_type   # *args
                    )
                )
            for response in await asyncio.gather(*futures):
                pass

        return self._search_buf.copy()

    def reset_search_buf(self):
        self._search_buf.clear()

    def search(self, terms, search_type):
        """Search all unique terms in a database.

        search_type is an instance of SearchType or its subclass

        Returns a set Nodes containing the results data.
        """
        loop = asyncio.get_event_loop()
        nodes = loop.run_until_complete(self._search_async(terms, search_type))
        return nodes

    def search_node_refs(self, node):
        """Search for all references of a node and return them as set of Nodes.

        After calling, node.ref_ids should be populated.
        """
        raise NotImplementedError("Method 'search_node_refs' not implemented")

    def search_node_cites(self, node):
        """Search for all citations of a node and return them as set of Nodes.

        After calling, node.cite_ids should be populated.
        """
        raise NotImplementedError("Method 'search_node_cites' not implemented")


class DatabaseScopus(Database):
    """Database API dealing with Scopus."""

    _id_prefix = "2-s2.0-"

    def __init__(self, refresh=True, force=False, verbose=0, search_limit=20):
        self._refresh = refresh
        self._force = force
        self._verbose = verbose
        self.failed = []
        super().__init__(search_limit=search_limit)

    @staticmethod
    def date_from_doc(doc):
        """Extract a date from AbstractRetrieval or Document object."""
        y, m, d = doc.coverDate.split('-')
        y = min(max(int(y), datetime.MINYEAR), datetime.MAXYEAR)
        m = min(max(int(m), 1), 12)
        d = min(max(int(d), 1), 29) if y % 4 == 0 else min(max(int(d), 1), 28)
        return datetime.date(y, m, d)

    def create_node(self, doc, ref_ids=None, cite_ids=None):
        id_     = doc.eid
        id_type = 'eid'
        date    = self.date_from_doc(doc)
        title   = doc.title
        doi     = doc.doi if doc.doi is not None else "unknown"
        eid     = doc.eid
        try:
            # AbstractRetrieval
            label = doc.authors[0].surname.lower() + str(date.year)
        except AttributeError:
            # None or Document
            try:
                label = doc.author_names.split(',')[0].lower() + str(date.year)
            except AttributeError:
                label = "unknown" + str(date.year)
        except TypeError:
            # None
            label = "unknown" + str(date.year)
        try:
            # AbstractRetrieval
            url = doc.scopus_link
        except (AttributeError, TypeError):
            # Document or None
            url = "unknown"
        return NodeScopus(id_, id_type, date, label, title, doi, url, eid,
                          ref_ids=ref_ids)

    def _search_term(self, term, search_type):
        if type(search_type) is ScAbstract:
            try:
                res = AbstractRetrieval(
                    term, id_type=search_type.id_type,
                    view="FULL",
                    refresh=self._refresh
                )
            except ScopusException as e:
                if self._force:
                    warn("Failed to retrieve '{}'. Raised {}: {}"
                        .format(term, type(e), e))
                    return
                else:
                    raise(e)
        else:
            raise ValueError("Invalid search type: {}".format(search_type))

        if any([res.title is None, res.eid is None]):
            # publication is probably not on Scopus
            if self._verbose:
                warn("Term not found on Scopus: {}".format(term))
            with LOCK:
                self.failed.append(term)
            return

        with LOCK:
            try:
                ref_ids = {self._id_prefix+ref.id for ref in res.references}
            except TypeError:
                # None
                ref_ids = set()
            self._search_buf.add(self.create_node(res, ref_ids=ref_ids))

    def search_node_refs(self, node):
        if node.ref_ids is None:
            # node had not been searched for refs - try with AbstractRetrieval
            node = self.search([node], ScAbstract(node.id_type)).pop()
        return self.search(node.ref_ids, ScAbstract(node.id_type))

    def search_node_cites(self, node):
        search_res = ScopusSearch(
            "refeid(" + node.id + ")",
            view="COMPLETE",
            refresh=self._refresh
        )

        node.cite_ids = set(search_res.get_eids())
        return self.search(node.cite_ids, ScAbstract(node.id_type))


class Node:
    """A graph node representing a single publication"""

    def __init__(self, id_, id_type, date, label, title, doi, url,
                 ref_ids=None, cite_ids=None):
        self.id = id_
        self.id_type = id_type
        self.date = date
        self.label = label
        self.title = title
        self.doi = doi
        self.url = url
        self.ref_ids = ref_ids
        self.cite_ids = cite_ids

    def __hash__(self):
        return hash(self.id)

    def __lt__(self, other):
        return self.date < other.date

    def has_refs(self):
        return self.ref_ids is not None

    def has_cites(self):
        return self.cite_ids is not None


class NodeScopus(Node):
    """A graph node representing a single publication"""

    def __init__(self, id_, id_type, date, label, title, doi, url, eid,
                 ref_ids=None, cite_ids=None):
        self.id = id_
        self.id_type = id_type
        self.date = date
        self.label = label
        self.title = title
        self.doi = doi
        self.url = url
        self.eid = eid
        self.ref_ids = ref_ids
        self.cite_ids = cite_ids


class NodePile:
    """Pile of Nodes that counts their number of occurences."""

    def __init__(self, has_graph=False):
        self._pile = set()  # unorganized set of all node.id-s
        self._counts = OrderedDict()  # dict of { node.id: citation_count }
        self._graph = nx.DiGraph() if has_graph else None  # graph of nodes

    def _increment_node(self, node_id):
        """Increment node count or add the entry if not present"""
        try:
            self._counts[node_id] += 1
        except KeyError:
            self._counts[node_id] = 1

    def _sort(self):
        """Sort the node pile 1. by year, 2. by citation count"""
        sort_fn = lambda k: (NODE_LUT[k].date.year, self._counts[k])
        self._counts = OrderedDict(
            {k: self._counts[k] for k in sorted(self._counts, key=sort_fn,
                                                reverse=True)}
        )

    def reset(self):
        """Reset to default state"""
        self._pile.clear()
        self._counts.clear()
        if self._graph is not None:
            self._graph.clear()

    def update_pile(self, nodes):
        """Add nodes to the node pile and global node LUT"""
        NODE_LUT.update_nodes(nodes)
        for node in nodes:
            self._pile.add(node.id)

    def _add_graph_node(self, node):
        if self._graph is not None:
            self._graph.add_node(node)

    def _add_graph_edge(self, start, end):
        if self._graph is not None:
            self._graph.add_edge(start, end)

    def search_internal_refs(self):
        """Search all references within the node pile"""

        for node_id in self._pile:         # for every node in pile
            node = NODE_LUT[node_id]
            self._add_graph_node(node.id)
            for ref_id in node.ref_ids:    # for every reference of that node
                if ref_id in self._pile:   # reference was cited in the pile
                    self._increment_node(ref_id)
                    self._add_graph_edge(node.id, ref_id)

        self._sort()

    def draw_graph(self):
        if self._graph is not None:
            labels_lut = {nid: node.label for nid, node in NODE_LUT.items()}
            nx.draw(self._graph, with_labels=True, labels=labels_lut)
            plt.show()

    def report(self, exclude_terms=None, exclude_type='doi'):
        node_labels = [NODE_LUT[nid].label for nid in self._counts.keys()]
        maxlen0 = len(max(node_labels, key=len)) if node_labels else 0
        maxlen1 = len(str(max(self._counts.values()))) if node_labels else 0

        for nid, cnt in self._counts.items():
            node = NODE_LUT[nid]
            exclude_term = getattr(node, exclude_type)
            if not exclude_terms or exclude_term not in exclude_terms:
                print("{0:{maxlen0}} {1:{maxlen1}} : {2}"
                    .format(node.label, cnt, node.title, maxlen0=maxlen0,
                            maxlen1=maxlen1))

        self.draw_graph()

    def write_csv(self, csv_file, exclude_terms=None, exclude_type='doi'):
        print("Writing to file '{}'".format(csv_file))
        with open(csv_file, 'w') as wf:
            wf.write("year,internal_cites,title,doi,url\n");
            for nid, cnt in self._counts.items():
                node = NODE_LUT[nid]
                exclude_term = getattr(node, exclude_type)
                if not exclude_terms or exclude_term not in exclude_terms:
                    wf.write('"')
                    wf.write('","'.join([str(node.date.year), str(cnt),
                                         node.title, node.doi, node.url]))
                    wf.write('"')
                    wf.write('\n')


class Searcher:
    def __init__(self, databases):
        self._dbs = databases

    def search_all(self, nodes, levels, has_graph=False):
        """Search all references of all nodes up to number of levels"""
        node_pile = NodePile(has_graph=has_graph)
        node_pile.update_pile(nodes)
        node_pile.search_internal_refs()
        db = self._dbs[0]

        for level in range(levels):
            node_cnt = 0
            ref_cnt = 0
            cite_cnt = 0
            ref_nodes_lvl = set()
            cite_nodes_lvl = set()
            if level > 0:
                print('', flush=True)
            for node in nodes:
                node_cnt += 1
                endc = '\r' if node_cnt != len(nodes) else '\n'
                print("Level {}, searching for references of {} nodes: {}/{}"
                      .format(level+1, len(nodes), node_cnt, len(nodes)),
                      flush=True, end='')
                ref_nodes = db.search_node_refs(node)
                ref_cnt += len(ref_nodes)
                cite_nodes = db.search_node_cites(node)
                cite_cnt += len(cite_nodes)
                print(", Found: {} refs and {} citations"
                      .format(ref_cnt, cite_cnt), end=endc)
                node_pile.update_pile(ref_nodes)
                ref_nodes_lvl.update(ref_nodes)
                node_pile.update_pile(cite_nodes)
                cite_nodes_lvl.update(cite_nodes)
            nodes = ref_nodes_lvl
            node_pile.search_internal_refs()

        return node_pile


def main():
    # Command line arguments
    args = cli()

    # Read DOIs from file or stdin
    if has_stdin():
        try:
            args.i.insert(0, sys.stdin)
        except AttributeError:
            args.i = [sys.stdin]
    if not args.i:
        return err("No input provided.")
    dois = read_dois(args.i)

    # Read the exclude file
    if args.exclude:
        with open(args.exclude, 'r') as rf:
            exclude_type = next(rf).strip()
            exclude_set = {line.strip() for line in rf}
    else:
        exclude_type = 'doi'
        exclude_set = None

    # Setup databases
    db_scopus = DatabaseScopus(args.nocache, args.force, args.verbose,
                               args.search_limit)

    # Setup a searcher
    searcher = Searcher([db_scopus])

    # Search
    nodes = db_scopus.search(dois, ScAbstract('doi'))
    node_pile = searcher.search_all(nodes, args.levels, args.graph)

    # Output
    NODE_LUT.relabel()
    node_pile.report(exclude_set, exclude_type)
    if args.csv:
        node_pile.write_csv(args.csv, exclude_set, exclude_type)


if __name__ == "__main__":
    sys.exit(main())
